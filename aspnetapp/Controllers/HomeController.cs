﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using aspnetapp.Models;

namespace aspnetapp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Message"] = "V2 Welcome to the docker demo homepage.";
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "V2 Welcome to the docker demo about page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "V2 Welcome to the docker demo contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            ViewData["Message"] = "V2 Welcome to the docker demo privacy page.";

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
